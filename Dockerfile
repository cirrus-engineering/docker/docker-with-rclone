FROM docker:git

LABEL maintainer Travis Reynolds <travis@cirruscreative.co.uk>

RUN apk update && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/* \
    && cd /tmp \
    && wget -q https://downloads.rclone.org/rclone-current-linux-amd64.zip \
    && unzip /tmp/rclone-current-linux-amd64.zip \
    && mv /tmp/rclone-*/rclone /usr/bin/ \
    && rm -r /tmp/rclone*.zip \
    && rm -rf /tmp/rclone*
